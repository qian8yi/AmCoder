## 查看一些MySQL数据库的性能参数

```SQL
SHOW STATUS;
```

### 查看某项具体参数

```SQL
show status like '%value%'
-- 其中value可以为以下参数：
  -- Connections：连接mysql服务器的次数  
  -- Uptime：mysql服务器的上线时间  
  -- Slow_queries：慢查询的次数  
  -- Com_select：查询操作的次数  
  -- Com_insert：插入操作的次数  
  -- Com_update：更新操作的次数  
  -- Com_delete：删除操作的次数  
```

> 示例一说明：查询mysql服务器的连接次数

```SQL
SHOW STATUS LIKE 'Connections';
```
![连接数](https://s1.ax1x.com/2020/07/30/aunY9K.png)

> 示例一说明：查询mysql服务器的慢查询次数
>> 查询mysql服务器的慢查询次数。慢查询次数参数可以结合慢查询日志，找出慢查询语句，然后针对慢查询语句进行表结构优化或者查询语句优化

```SQL
SHOW STATUS LIKE 'Slow_queries';
```
![慢查询](https://s1.ax1x.com/2020/07/30/auMp6O.png)

## 分析查询语句

> 通过分析查询语句，可以了解查询语句的执行情况，找出查询语句的执行瓶颈，从而优化查询语句；mysql中提供了EXPLAIN语句和DESCRIBE语句，用来分析查询语句  

- EXPLAIN语句的基本语法如下：  
  - EXPLAIN [EXTENDED] SELECT select_options;  
  - 使用EXTENED关键字,EXPLAIN语句将产生附加信息。select_options是select语句的查询选项，包括from where子句等等。  
  - 执行该语句，可以分析EXPLAIN后面的select语句的执行情况，并且能够分析出所查询的表的一些特征。

```SQL
-- 示例说明
EXPLAIN SELECT * FROM user;
```
![EXPLAIN示例](https://s1.ax1x.com/2020/07/30/auQrqg.png)

!> 查询结果说明

  - id：select识别符，这是select的查询序列号。  
  - select_type：标识select语句的类型。  
    - 它可以是以下几种取值：  
      - b1、SIMPLE（simple）表示简单查询，其中不包括连接查询和子查询。  
      - b2、PRIMARY（primary）表示主查询，或者是最外层的查询语句。  
      - b3、UNION（union）表示连接查询的第2个或者后面的查询语句。  
      - b4、DEPENDENT UNION（dependent union）连接查询中的第2个或者后面的select语句。取决于外面的查询。   
      - b5、UNION RESULT（union result）连接查询的结果。  
      - b6、SUBQUERY（subquery）子查询的第1个select语句。  
      - b7、DEPENDENT SUBQUERY（dependent subquery）子查询的第1个select,取决于外面的查询。
      - b8、DERIVED（derived）导出表的SELECT(FROM子句的子查询)   。  
  - type：表示查询的表。  
  - type：表示表的连接类型。  
    - 下面按照从最佳类型到最差类型的顺序给出各种连接类型。  
      - d1、system，该表是仅有一行的系统表。这是const连接类型的一个特例   
      - d2、const，数据表最多只有一个匹配行，它将在查询开始时被读取，并在余下的查询优化中作为常量对待。const表查询速度很快，因为它们只读一次。const用于使用常数值比较primary key或者unique索引的所有部分的场合。例如：`EXPLAIN SELECT * FROM user WHERE id=1;`
      - d3、eq_ref，对于每个来自前面的表的行组合，从该表中读取一行。当一个索引的所有部分都在查询中使用并且索引是UNIQUE或者PRIMARY KEY时候，即可使用这种类型。eq_ref可以用于使用“=”操作符比较带索引的列。比较值可以为常量或者一个在该表前面所读取的表的列的表达式。例如：`EXPLAIN SELECT * FROM user,db_company WHERE user.company_id = db_company.id;`  
      - d4、ref对于来自前面的表的任意行组合，将从该表中读取所有匹配的行。这种类型用于所以既不是UNION也不是primaey key的情况，或者查询中使用了索引列的左子集，即索引中左边的部分组合。ref可以用于使用=或者<=>操作符的带索引的列。  
      - d5、ref_or_null，该连接类型如果ref，但是如果添加了mysql可以专门搜索包含null值的行，在解决子查询中经常使用该连接类型的优化。  
      - d6、index_merge，该连接类型表示使用了索引合并优化方法。在这种情况下，key列包含了使用的索引的清单，key_len包含了使用的索引的最长的关键元素。  
      - d7、unique_subquery，该类型替换了下面形式的in子查询的ref。是一个索引查询函数，可以完全替代子查询，效率更高。  
      - d8、index_subquery，该连接类型类似于unique_subquery，可以替换in子查询，但是只适合下列形式的子查询中非唯一索引   
      - d9、range，只检索给定范围的行，使用一个索引来选择行。key列显示使用了那个索引。key_len包含所使用索引的最长关键元素。当使用=，<>,>,>=,<,<=,is null,<=>，between或者in操作符，用常量比较关键字列时，类型为range。  
      - d10、index,该连接类型与all相同，除了只扫描索引树。着通常比all快，引文索引问价通常比数据文件小。  
      - d11、all，对于前面的表的任意行组合，进行完整的表扫描。如果表是第一个没有标记const的表，这样不好，并且在其他情况下很差。通常可以增加更多的索引来避免使用all连接。     
  - possible_keys:possible_keys列指出mysql能使用那个索引在该表中找到行。如果该列是null，则没有相关的索引。在这种情况下，可以通过检查where子句看它是否引起某些列或者适合索引的列来提高查询性能。如果是这样，可以创建适合的索引来提高查询的性能。  
  - key：表示查询实际使用到的索引，如果没有选择索引，该列的值是null，要想强制mysql使用或者忽视possible_key列中的索引，在查询中使用force index、use index或者ignore index。  
  - key_len：表示mysql选择索引字段按照字节计算的长度，如果健是null，则长度为null。注意通过key_len值可以确定mysql将实际使用一个多列索引中的几个字段。  
  - ref:表示使用那个列或者常数或者索引一起来查询记录。  
  - rows:显示mysql在表中进行查询必须检查的行数。  
  - Extra:该列mysql在处理查询时的详细信息。

- DESCRIBE语法分析

> DESCRIBE语句的使用方法与EXPLAIN语句是一样的，并且分析结果也是一样的。

- DESCRIBE语句的语法形式如下所示(**DESCRIBE可以缩写成DESC**)：
```SQL
DESCRIBE SELECT select_options
```
