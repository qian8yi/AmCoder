!> 解释一些经常遇见的名称或者概念，避免用名词解释名词，有时候虽然解释的不是很官方，但却很实用

- [强弱-静动类型语言](名词概念解释/强弱-静动类型语言.md)
- [值类型和引用类型](名词概念解释/值类型和引用类型.md)
- [DML-DDL-DCL](名词概念解释/DML-DDL-DCL.md)
- [Iass-Pass-SasS](名词概念解释/Iass-Pass-SasS.md)
- [Java重写和重载](名词概念解释/Java重写和重载.md)
- [Session和Cookie](名词概念解释/Session和Cookie.md)
