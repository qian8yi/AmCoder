**DML/DDL/DCL**

- DML(Data Manipulation Language)数据操纵语言
> 就是我们最经常用到的 SELECT、UPDATE、INSERT、DELETE

- DDL(Data Definition Language)数据库定义语言
> 就是我们在创建表的时候用到的一些sql，比如说：CREATE、ALTER、DROP等。DDL主要是用在定义或改变表的结构，数据类型，表之间的链接和约束等初始化工作上

- DCL(Data Control Language)数据库控制语言
> 是用来设置或更改数据库用户或角色权限的语句，包括（grant,deny,revoke等）语句
