<!-- ![logo](https://docsify.js.org/_media/icon.svg) -->
![logo](https://s1.ax1x.com/2020/05/20/YojfYt.gif "-gifcontrol-disabled;")

<!-- ## :point_right: AmCoder个人笔记 :point_left: -->

## 主要分享一些在工作或者学习中所遇到的一些问题以及记录一些学习到的东西等！

#### :point_right: [常用标签地址](GoodLink.md)、[干货博客地址](GoodBlog.md)、[文章随笔记录](文章随笔记录/README.md)、[名词概念解释](名词概念解释/README.md)、[学习系列记录](学习系列记录/README.md)、[环境安装系列](环境安装系列/README.md)、[代码实战记录](代码实战记录/README.md)、[常用命令总结](常用命令总结/README.md)、[常见问题总结](常见问题总结/README.md):point_left:

:star: 如果您喜欢请[![star](https://gitee.com/AmCoder/AmCoder/badge/star.svg?theme=dark)](https://gitee.com/AmCoder/AmCoder/stargazers)给予关注，谢谢 :star:

:movie_camera: [***大数据开源社区10年发展史***](https://v.youku.com/v_show/id_XNDM1MDI1MjU0NA==.html?spm=a2h9p.12366999.app.SECTION~MAIN~SECTION~MAIN~5~5!2~5!3~5~5~5~5~5~21~22~TR~TD!2~5~5!2~H4~A)

[快速开始](README.md)
