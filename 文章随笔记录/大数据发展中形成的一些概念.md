!> 作者：***AmCoder***，本文只是想针对这些年在大数据圈子中经常出现的概念做一些总结和记录，纯属个人观点与认知。如果有理解错误的地方还请告知。

# 写作背景

> 工作中或者新闻中我们偶尔或者经常听说一些词汇，**大数据** **BI** **数据仓库** **数据湖** **数据集市** **数据中台** 等，这些词汇到底是如何诞生的？每个词汇中又包含着什么样的内容？本文将重点总结。

# 简单说说

## 大数据起源

> **大数据** 这一词大概是1980年提出，在2008年一个名为《自然》的杂志中首次提出了 **大数据** 的概念，而最早应用于一个著名的咨询公司 "麦肯锡"，进入到2013年，有媒体将本年成为 **大数据元年** (何为元年？元年其中一个意思指的是为纪年的第一年)，也就是意味着，真正的大数据时代来临了。

## 大数据到底有多大？

> 处在大数据时代，人们的第一反应就是大数据就是数据量大，这是从字面意思获取到的一个 **大数据** 的特点。据统计，一分钟内，仅微博网站推特上新发的信息条数就超过10万条；社交网络脸谱上的浏览量则超过600万；到2013年，全世界储存的信息如果记录在光盘上，这些光盘叠加起来，高度等于从地球到月球走一趟。

## 个人对大数据的理解

> 我个人认为，如果一定要给大数据一个合理的解释，可以试着将大数据分为 **"大"** 和 **"数据"**，如何去理解呢？首先理解一下 **"大"** 这个字
  >> **大** 在百度汉语中给出的基本释义为：指面积、体积、容量、强度、力量超过一般或超过所比较的对象,规模广，程度深，性质重要
    >>> 1. 与其说是面积体积，容量等不如说是 **"数据多样性种类"** 的大，比如常见的结构化数据，非结构化数据和半结构化数据等。
    >>> 2. 与其说规模广，不如说是 **维度广** ，比如，可以从一个人的各种数据属性中进行分析出不同的维度。
    >>> 3. 与其说是性质重要，不如说是 **分析数据结果** 的重要，比如说结合历史数据预测未来的某件事情。
    >>> 4. 字面意义上的数据量大。
    >>> 5. 即时性，通过快速分析实时得到一些重要的指标或者信息。

  >> **数据** 在百度汉语中的释义为：数据就是数值，也就是我们通过观察、实验或计算得出的结果。数据有很多种，最简单的就是数字。数据也可以是文字、图像、声音等。数据可以用于科学研究、设计、查证等。
  >>> 1. 文字，图片，声音，又一次充分说明了大数据的 **数据多样性**
  >>> 2. 通过观察，实验或者计算得出的结果，可以说明，大数据中的数据是 **有价值的数据** (是否有价值，这个只能相对来说，因为不同的人所看的角度不一样，产生的价值观自然而然也就不一样了)

- 以上仅是个人观点，当然在百度百科中的大数据概念中早已总结为以下5个特征
  - 容量（Volume）：数据的大小决定所考虑的数据的价值和潜在的信息
  - 种类（Variety）：数据类型的多样性
  - 速度（Velocity）：指获得数据的速度
  - 可变性（Variability）：妨碍了处理和有效地管理数据的过程
  - 真实性（Veracity）：数据的质量
  - 复杂性（Complexity）：数据量巨大，来源多渠道
  - 价值（value）：合理运用大数据，以低成本创造高价值

> 其中IBM提出提出了5V特点：Volume（大量）、Velocity（高速）、Variety（多样）、Value（低价值密度）、Veracity（真实性）。


# 大数据的技术架构发展历程(又或者说是阶段)

!> 决策支持系统(DSS)<br/>数据仓库(Data WareHouses)<br/>数据集市(Data Mart)<br/>数据湖(Data Lake)<br/>数据中台<br/>商业智能(BI)

## 决策支持系统(DSS)

### 定义

> 决策支持系统（**Decision Support Systems，简称DSS**)是协助进行 **商业级或组织级决策活动** 的信息系统。DSSs一般面向中高层面管理，服务于组织机构内部管理、操作和规划级的决策，帮助决策者对快速变化并且很难提前确定的问题进行决策，通常是 **非结构化（Non-structured）和半结构化（Semi-structured）** 的决策问题。决策支持系统既可以是 **完全自动化决策，也可以是完全人工决策** ，或者两者兼有。 --- 来自维基百科

- 解读一下上面那段话
  1. **商业及或者组织级** 说明决策支持系统是协助某个团体或者某个单位而非单个人，**商业** 说明系统的使用场景是在可以获取利益下的场景中使用的。
  2. **非结构和半结构化** 说明决策支持系统支持的数据类型为 **非结构化** 和 **半结构化** 数据。
  3. **完全自动化决策，也可以是完全人工决策** 说明决策支持系统可以是系统自动决策(比如某个模型分析的结果，又或者是某些数据得到的结果)也可以是完全人工决策(某个单位或者某个组织商讨决策)

### 历史

> DSS的概念则于1970年代开始形成，并在1980年代蓬勃发展，由于过去的DSS定义相当广泛，因此1990年Turban则进一步以DSS的特性来定义
![Turban对于决策系统的定义](https://s1.ax1x.com/2020/06/23/NUPijS.png)

> 1990年代起，数据仓库与OLAP的概念也导入至DSS，协助DSS进行数据的访问与分析。

> 2000年代新的万维网、网络技术与互联网，延展了DSS。

### 架构或者特点

> DSS的架构以Sprague与Carlson所提出的 **对话-数据-模式（Dialog-Data-Modeling，DDM）架构** 最为学术界所接受，认为DSS有三大组件：

- 数据库管理系统(Database Management System，**DBMS**)
  > 为管理数据库的工具，DSS的数据库包含大量内部数据（例如企业内部会计数据），或者外部数据（例如金融指数数据），这些数据需要经过搜集与萃取，成为有助于决策的信息形式与数据结构，以供用户进行管理、分析、更新与检索。

- 模式库管理系统(Model-base Management System，MBMS)
  > MBMS为集成各种决策模式，分析数据库内外部的数据，例如利用数学计量模式将复杂的问题加以分析模拟，提供可行之方案，并协助用户选择方案。MBMS也包含造模语言，协助用户自定义模式或建造模式。MBMS基本的必要条件包括了：</br>
    1. 能满足不同用户的模式需求
    2. 具有能集成模式与数据的能力
    3. 提供容易使用的接口
    4. 能够分享模式

- 对话产生与管理系统(Dialog Generation and Management System，DGMS)
  > 由于DSS等等特性，都由DSS与人类用户进行交互作用所产生，DGMS主要的功能为管理用户界面（User Interface）及DSS与用户交互。Bennett认为DGMS有三个主要构成单元：用户、电脑硬件与软件系统，并且将人类与DSS的相互沟通分为三个部分：</br>
    1. 行动语言（The Action Language）：指用户用做与DSS沟通的任何方式，如键盘、鼠标等任何控制硬软件的指令
    2. 显示或展示语言（Display or Presentation Language）：指用户可以由DSS所看到任何形式的输出信息，如屏幕、打印机或声音等
    3. 知识库（Knowledge Base）：指任何用户使用DSS所必须了解的知识，包含用户运用DSS必须知道才能有效使用的一切知识，如用户手册

## 数据仓库(Data WareHouses)

### 定义

> 数据仓库，英文名称为Data Warehouse，可简写为DW或DWH。数据仓库是决策支持系统（dss）和联机分析应用数据源的结构化数据环境。数据仓库研究和解决从数据库中获取信息的问题。数据仓库的特征在于面向主题、集成性、稳定性和时变性。 --- 来自百度百科

  - 解读一下
    1. **数据仓库是决策支持系统（dss）和联机分析应用数据源的结构化数据环境。** 可以说明是由上述dss演变而来，并配合以分析的数据的环境，共同组建为数仓。
    2. **数据仓库的特征在于面向主题、集成性、稳定性和时变性** 可以说明数仓的作用能实现跨业务条线、跨系统的数据整合，并且具备实时性和稳定性。

### 历史

> 数据仓库，由数据仓库之父比尔·恩门（Bill Inmon）于1990年提出，主要功能仍是将组织透过资讯系统之联机事务处理(OLTP)经年累月所累积的大量资料，透过数据仓库理论所特有的资料储存架构，做有系统的分析整理，以利各种分析方法如联机分析处理(OLAP)、数据挖掘(Data Mining)之进行，并进而支持如决策支持系统(DSS)、主管资讯系统(EIS)之创建，帮助决策者能快速有效的自大量资料中，分析出有价值的资讯，以利决策拟定及快速回应外在环境变动，帮助建构商业智能(BI)。（**总结一下就是利用某些技术，能够在大量的数据中获取有价值的信息**）

### 主要特点

- 面向主题，并且该主题通常和多个系统信息相关。
- **数仓的数源来自于不同数源，这些数据通过加工和一定的清洗之后才能进入数仓。**
- **数仓的数据主要是多用于决策分析使用，所以不可更新和删除，同时反之主要是用于查询。**
- 数仓的数据是随时间而变化的，并且是大容量的，故时间序列数据集合非常大。
- **非规范化的，数仓的数据经常是冗余的。**
- **数据质量要保证，避免因清洗代码不严谨而导致的脏数据，这样分析出的结果也就偏离了**
- **具备扩展性，不要因为考虑到是热门技术而草率决定和搭建，要至少考虑到为了3-5年的扩展性**

!> 上述加粗的特点这些送给不明白使用场景的朋友，不要把数仓当成结构化的DBMS使用。

## 数据集市(Data Mart)
> 数据集市(Data Mart) ，也叫数据市场，数据集市就是满足特定的部门或者用户的需求，按照多维的方式进行存储，包括定义维度、需要计算的指标、维度的层次等，生成面向决策分析需求的数据立方体。 --- 来自百度百科

- 数据集市和数仓的区别
  - **数据集市可以理解为数仓的一个子集，主要是面向部门级业务，并且只面向某个特定的主题。**
  - 特征规模小
  - 结构通常被描述为星型或者是雪花型结构，一个星型包含两个基本部分，一个事实表和各种支持表。

## 数据湖(Data Lake)

- 上述的数仓的数据挖掘需要预先进行脏数据处理，数据清洗加工等，并且只支持结构化数据，随着时间的演变，分析时面临着这两个问题，所以就引出了数据湖的概念。
  - 如果只是使用一部分属性或者特征，这些数据只能分析出预定好的问题。
  - 数据进行了聚合分析，那么某些最低层级的细节就会丢失，所分析出的结果自然而然被限制了
  - 如何解决数据孤岛问题（专业人士把数据孤岛分为物理性和逻辑性两种。物理性的数据孤岛指的是，**数据在不同部门相互独立存储，独立维护，彼此间相互孤立，形成了物理上的孤岛。**---来自百度百科）

> 数据湖（Data Lake）是一个以原始格式存储数据的存储库或系统。它按原样存储数据，而无需事先对数据进行结构化处理。一个数据湖可以存储结构化数据（如关系型数据库中的表），半结构化数据（如CSV、日志、XML、JSON），非结构化数据（如电子邮件、文档、PDF）和二进制数据（如图形、音频、视频）。 --- 来自维基百科

- 解读一下
  - **它按原样存储数据，而无需事先对数据进行结构化处理** 这点有区别于数仓。

### 历史

> 数据湖最早是由Pentaho的创始人兼CTO， **James Dixon，在2010年10月纽约Hadoop World大会上提出来的**。当时Pentaho刚刚发布了Hadoop的第一个版本。在这样的一个大背景下，可以合理的猜测，当时James Dixon提出数据湖的概念，是为了推广自家的Pentaho产品以及Hadoop的。

### 主要特点(区别于数仓)

- **数据湖与数据仓库的一大区别就是，Schema On Read，即在使用数据时才需要Schema信息；而数据仓库是Schema On Write，即在存储数据时就需要设计好Schema。这样，由于对数据写入没有限制，数据湖可以更容易的收集数据。**

- **数据仓库和数据集市由于只使用数据中的部分属性，所以只能回答一些事先定义好的问题；而数据湖存储所有最原始、最细节的数据，所以可以回答更多的问题。并且数据湖允许组织中的各种角色通过自助分析工具，对数据进行分析，以及利用AI、机器学习的技术，从数据中发掘更多的价值。**

- **消除数据孤岛：数据湖中汇集了来自各个系统中的数据，这就消除了数据孤岛问题。**

- 数据湖可以利用分布式文件系统来存储数据，因此具有很高的扩展能力。开源技术的使用还降低了存储成本。**数据湖的结构没那么严格，因此天生具有更高的灵活性，从而提高了敏捷性**

### 总结
> 数据湖相比于数仓来说具备更强的扩展能力和灵活性，不用像数仓一样在存储是需要考虑数据结构，并且可以汇入多种不同数据源结构化或者非结构化的数据，当然分析的维度也就不只是固定的，而是具备一定灵活性的。
>> 但是数据湖的理念虽然好，但是却在实际应用中缺乏可操作性的使用产品支撑，比如：
>>> **数据沼泽** 当越来越多的不同数源汇入了数据湖，但是由于没有有效的方法跟踪或分析数据，导致许多人会产生一种思想，先将数据汇集，期望以后可以挖掘什么，可没有多久就忘记了汇集了哪些数据，就会产生数据沼泽的概念。</br> **数据泥团** 由于各种各样的数据汇集到的数据湖，它们的组织形式不同，质量不同，苦于缺乏用于检查，清理和重组数据的服务工具，使得这些数据很难创造价值。</br>**缺乏自助分析工具**</br>**缺乏建模的方法论和工具**</br>**缺少数据安全管理**</br>**一个数据湖就够了**

### 目前开源的数据湖组件

1. Delta Lake是Databricks公司今年四月刚刚开源的一个项目。它基于自家的Spark，为数据湖提供支持ACID事务的数据存储层。主要功能包括：支持ACID事务、元数据处理、数据历史版本、Schema增强等。

2. Kylo是Teradata开源的一个全功能的数据湖平台。它基于Hadoop和Spark。提供一套完整的数据湖解决方案，包括数据集成、数据处理、元数据管理等功能。功能比较齐全。

3. Dremio是Dremio公司开源的一个DaaS平台。它主要基于Apache Arrow，提供基于Arrow的执行引擎，使得数据分析师可以对多种数据源的数据进行联合分析。

> 除此之外，还有一些商业的数据湖平台，比如zaloni。另外，各大云厂商也都提供了数据湖平台或数据湖分析服务，比如Azure、Amazon、阿里云等。


## 数据中台

> 数据中台是指通过企业内外部多源异构的数据采集、治理、建模、分析，应用，使数据对内优化管理提高业务，对外可以数据合作价值释放，成为企业数据资产管理中枢。数据中台建立后，会形成数据API，为企业和客户提供高效各种数据服务。<br/>
中台战略核心是数据服务的共享。**中台战略并不是搭建一个数据平台，但是中台的大部分服务都是围绕数据而生，数据中台是围绕向上层应用提供数据服务构建的，中台战略让数据在数据平台和业务系统之间形成了一个良性的闭环，也就是实现应用与数据之间解藕，并实现紧密交互。**

- 敏捷前台：一线作战单元，强调敏捷交互及稳定交付的组织能力建设。

- 业务中台：能力固化与赋能，固化通用能力，赋能前线部队，提升配置效率，加快前线响应，产品化业务化，开辟全新生态。

- 数据中台：资产整合与共享，整合多维数据，统一资产管理，连通数据孤岛，共享数据资源，深入挖掘数据，盘活资产价值。

- 稳定后台：以共享中心建设为核心，为前中台提供专业的内部服务支撑。

- 解读一下
  - **企业内外部多源异构的数据采集、治理、建模、分析，应用** 针对企业内外部不同数据源或不同结构的数据进行采集，加工，分析并应用
  - **数据中台建立后，会形成数据API** 指的是以API的方式进行向外提供

### 历史

> 2019年，数据中台成为大数据行业的热门概念，它最先是从阿里引出的，是指通过数据技术，**对海量数据进行采集、计算、存储、加工，同时统一标准和口径。数据中台把数据统一之后，会形成标准数据，再进行存储，形成大数据资产层，进而为客户提供高效服务。** ***这些服务跟企业的业务有较强的关联性，是这个企业独有的且能复用的，*** 它是企业业务和数据的沉淀，其不仅能降低重复建设、减少烟囱式协作的成本，也是差异化竞争优势所在。

### 主要特点和总结

- 数据中台的构成包含了数据仓库（用于存储数据）、大数据中间件（用于分析和展现等）、资产数据管理
- 数据中台不是一个套件或者工具而是一个信息系统
- 数据中台更好的支撑数据预测分析、跨领域分析、主动分析、实时分析、多元化结构化数据分析
- 用一张架构图说明一下
![数据中台架构图](https://s1.ax1x.com/2020/06/30/N5S8Qx.jpg)

### 故事 ---> 由来
!> 早在 2015 年，马云就提出过这个概念。马云当时去芬兰参访一家叫 Supercell 的游戏公司，深受启发。这家公司的员工不到 200 人，但一年利润却有惊人的 15 亿美金，平均每人的产值高达 750 万美元。而每一个开发游戏的小团队，只有六七人。这么小的团队，这么强大的能力，赚这么多的钱，怎么做到的？<br/><br/>一个很重要的原因，是 Supercell 把开发游戏中那些大量重复的工作整理出来，变成统一的工具提供给所有人。这些工具里面就包含通用的游戏素材，游戏算法等等，一套工具，能支持所有团队，开发速度快，工作效率高。这样的管理和协作方式，就算是一种 **「中台」**。<br/><br/>马云，深受启发。不久之后，阿里就确定了「中台战略」，要搭建「数据中台」，把底层的用户数据、交易数据等等全部打通。这些数据可以联动共享，把这些数据统一放到一个平台，可以支持指导其他业务

# 终极目标(ps：make more and more money)

## 商业智能(BI)

### 定义

> 商业智能（Business Intelligence，简称：BI），又称商业智慧或商务智能，指用现代数据仓库技术、线上分析处理技术、数据挖掘和数据展现技术进行数据分析以实现商业价值。 --- 来自百度百科

- 解读一下
  1. 通过某种当代大数据的技术手段进行数据分析或者是挖掘某种有意义有价值的数据来获取利益。

### 历史

> 商业智能的概念在1996年最早由加特纳集团（Gartner Group）提出.
>> 在1989年，Howard Dresner将商业智能描述为 **"使用基于事实的决策支持系统"** ，来改善业务决策的一套理论与方法。（ps:可见商业智能是基于决策支持系统之上而构建的）
>>> 商业智能通常被理解为将企业中现有的数据转化为知识，帮助企业做出明智的业务经营决策的工具。这里所谈的数据包括来自企业业务系统的订单、库存、交易账目、客户和供应商等来自企业所处行业和竞争对手的数据以及来自企业所处的其他外部环境中的各种数据。而商业智能能够辅助的业务经营决策，既可以是操作层的，也可以是战术层和战略层的决策。为了将数据转化为知识，***需要利用数据仓库***、***联机分析处理（OLAP）工具*** 和 ***数据挖掘等技术***。**因此，从技术层面上讲，商业智能不是什么新技术，它只是数据仓库、OLAP和数据挖掘等技术的综合运用。**

# 个人心得

> 上述介绍了由最初的决策支持系统到数据仓库，数据集市，数据湖和数据中台的概念，其中有各个概念的定义，有历史，有区别，以及使用背景，所以在我们搭建某个架构时需要三思而后行，好的架构并非一拍头脑或者当下流行度而决定的，适用自己公司的才是最好的，当然从利益角度考虑这一些的背后技术手段的目的都是为了商业智能而铺垫。而从技术角度考虑，是为了解放更多的繁琐反复的工作。让我们的工作更具有效率性，稳定性和安全性。冰冻三尺非一日之寒，水滴石穿非一日之功。for big data architect。
