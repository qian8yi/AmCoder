- 问题描述：树莓派在vi编辑状态下左右方向键和退格键 **"失灵"**

**解决方案**

> 使用root权限打开/etc/vim/vimrc.tiny </br>编辑vimrc.iny中的set compatible 为 set nocompatible</br>编辑vimrc.iny，添加 set backspace=2
